import axios from 'axios'
// var header_variables = {
//     'Authorization': `${window.localStorage.getItem('token')}`,
//     'Content-Type': 'application/json'
// }

const HTTP = axios.create({
    baseURL: `/api/`,
    // headers: header_variables

})

export const testGet = ({
    commit,
    state
}, pin) => {
    commit('setLoading', true);
    return HTTP.get('test').then((response) => {
        commit('setLoading', false);
        console.log('resp ' + response.data.resp);
    }, (err) => {
        console.log("error : " + err);
    });
}