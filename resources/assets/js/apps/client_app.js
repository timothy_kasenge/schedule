/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('../bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import VueRouter from 'vue-router'


// router setup
import routes from '../routes/client_route'

// vuex setup
// store
import store from '../store/client'

Vue.use(VueRouter)

// configure router
const router = new VueRouter({
    // mode: 'history',
    routes, // short for routes: routes
    linkActiveClass: 'active'
})

const app = new Vue({
    router,
    store
}).$mount('#app')