@extends('layouts.app')

@section('content')
<div class="container">
    <transition name="fade" mode="out-in">
        <!-- your content here -->
        <router-view></router-view>
    </transition>   
</div>
@endsection